<?php
return [
    'dependencies' => [
        'invokables' => [
            \EcgMonClient\Command\HelloCommand::class => \EcgMonClient\Command\HelloCommand::class,
        ],

        'factories' => [
            \Symfony\Component\Console\Application::class => EcgMonClient\ApplicationFactory::class,
        ],
    ],
];
