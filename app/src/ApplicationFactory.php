<?php
namespace EcgMonClient;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Symfony\Component\Console\Command\Command;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class ApplicationFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');
        $app = new Application;

        $commands = $config['app']['commands'] ?? array();

        foreach ($commands as $command) {
            if (is_string($command)) {
                $command = $container->get($command);
            }

            if (!($command instanceof Command)) {
                throw new ServiceNotCreatedException(sprintf('Command needs to be an instance of %s, got %s',
                    Command::class, get_class($command)));
            }

            $app->add($command);
        }

        return $app;
    }
}