<?php
namespace EcgMonClient;

class Application extends \Symfony\Component\Console\Application
{
    const APPLICATION_NAME = 'ECG-Mon Client';
    const APPLICATION_VERSION = '0.1-dev';

    public function __construct($name = null, $version = null)
    {
        parent::__construct($name ?? self::APPLICATION_NAME, $version ?? self::APPLICATION_VERSION);
    }


}